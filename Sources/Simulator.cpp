#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <ctime>

#include "Simulator.h"
#include "Scheduler.h"
#include "Node.h"
#include "Task.h"
#include "Config.h"
#include "RNG.h"
#include "TimeManager.h"

using namespace std;

Simulator::Simulator() : fScheduler(nullptr) {
}

Simulator::~Simulator() {
}

void Simulator::Run() {
	//first read simulation data
	ReadData();
	
	//second init report values
	Init();

	cerr << "Simulation started .." << endl;
	
	//create nodes list
	list<Node*> nodes;

	for(int i = 0; i < fTotalNodes; ++i) {
		int area = RNG::RandomBetween(fNodeLowArea, fNodeHighArea);
		auto node = new Node(i+1, area);
		nodes.push_back(node);
	}
	
	//create also the configs list
	list<Config*> configs;
	
	//using fTotalConfigs
	for(int i = 0; i < fTotalConfigs; ++i) {
		int configTime = RNG::RandomBetween(fConfigTimeLow, fConfigTimeHigh);
		int requiredArea = RNG::RandomBetween(fTaskLowArea, fTaskHighArea);
		auto config = new Config(i+1, configTime, requiredArea);
		
		configs.push_back(config);
	}
	
	//now do a loop until max tasks generated reached
	
	//init the next task interval
	int nextTaskInterval = 0;
	while(true) {
		//increment time ticks
		TimeManager::GetInstance()->Tick();

		int t = TimeManager::GetInstance()->GetCurrentTime();

		if (t % 2000 == 0) {
			cerr << "t = " << t << endl;

			//display needed ticks to free nodes
			for (auto node : nodes) {
				cerr << "node = " << node->GetId()
					<< " max ticks to be idle = " << node->GetMaxRemainingTime()
					<< " state = " << node->GetState()
					<< " ava = " << node->GetAvailableArea()
					<< " tta = " << node->GetTotalArea() << endl;
			}

			//display suspended tasks count
			cerr << "suspended tasks count = " << fScheduler->GetSuspendedTasksCount() << endl;
		}
		
		//test if the max tasks is reached
		//test if the nextTaskInterval is reached generate new Task
		if(fTotalCurGenTasks < fTotalTasks) {
			if(nextTaskInterval == 0) {
				int prefConfig = RNG::RandomBetween(1, fTotalConfigs);
				int createdTime = TimeManager::GetInstance()->GetCurrentTime();
				int requiredTime = RNG::RandomBetween(fTaskReqTimeLow, fTaskReqTimeHigh);

				//increment task generated count
				++fTotalCurGenTasks;

				auto task = new Task(fTotalCurGenTasks, prefConfig, createdTime, requiredTime);

				cerr << "Task " << task->GetId() << " need " << requiredTime << " ticks." << endl;
				
				//schedule the created task 
				fScheduler->Schedule(task, nodes);
				
				// update nextTaskInterval
				nextTaskInterval = RNG::RandomBetween(fNextTaskMinInterval, fNextTaskMaxInterval);
			} else {
				//decrement nextTaskInterval
				--nextTaskInterval;
			}
		}
		
		//if the max task to generate is reached and scheduler
		//has finished its job with all task then simulation
		//must be terminated
		if(fTotalCurGenTasks == fTotalTasks) {
			//test if all nodes are idles and there is no suspended tasks
			bool done = true;
			for(auto node : nodes) {
				if(node->GetState() == eBusy) {
					done = false;
					break;
				}
			}
			
			if (done && (fScheduler->GetSuspendedTasksCount() == 0)) {
				cerr << "Simulation ended .." << endl;
				//system("PAUSE");
				break;
			}
		}
		
		//now update node status and the included tasks
		for(auto node : nodes) {
			node->Update();
		}

		//now try to re-schedule the suspended tasks
		fScheduler->RescheduleSuspendedTasks(nodes);
	}

	fTotalDiscardedTasks = fScheduler->GetTotalDiscardedTasks();

	fTotalSearchLengthScheduler = fScheduler->GetTotalSearchLength();

	fReConfigurationType = (fScheduler->GetReConfigurationType() == ePartial) ? "partial" : "full";

	fSimulationTime = TimeManager::GetInstance()->GetCurrentTime();

	fTotalTaskWaitTime = Task::sTotalTaskWaitingTime;
	
	//clear nodes
	for(auto node : nodes) {
		delete node;
	}
	
	//clear configs
	for(auto config : configs) {
		delete config;
	}

	//clear time manager
	TimeManager::ClearInstance();

	//clear node  static members
	Node::Clear();

	//clear config static members
	Config::Clear();

	//clear task static members
	Task::Clear();

	//clear scheduler
	delete fScheduler;
	fScheduler = nullptr;
}

void Simulator::MakeReport() {
	//create file name with current date to not have overwriting
	char fileName[50];
	time_t t = time(NULL);
	struct tm* now = localtime(&t);

	sprintf(fileName, "Reports/report_%04d_%02d_%02d_%02d_%02d_%02d.json",
		now->tm_year + 1900,
		now->tm_mon + 1,
		now->tm_mday,
		now->tm_hour,
		now->tm_min,
		now->tm_sec);

	ofstream out(fileName);

	/*
	out << fTimeTick << endl;
	out << fTotalTasks << endl;
	out << fTotalConfigs << endl;
	out << fTotalNodes << endl;
	out << fTotalCompletedTasks << endl;
	out << fTotalCurGenTasks << endl;
	out << fTotalCurSusTasks << endl;
	out << fTotalDiscardedTasks << endl;
	out << fTotalWastedArea << endl;
	out << fTotalSearchLengthScheduler << endl;
	out << fTotalTaskWaitTime << endl;
	out << fTotalTasksRunningTime << endl;
	out << fTotalConfigurationTime << endl;
	out << fNextTaskMaxInterval << endl;
	out << fNodeLowArea << endl;
	out << fNodeHighArea << endl;
	out << fTaskLowArea << endl;
	out << fTaskHighArea << endl;
	out << fTaskReqTimeLow << endl;
	out << fTaskReqTimeHigh << endl;
	out << fConfigTimeLow << endl;
	out << fConfigTimeHigh << endl;
	*/

	//for the moment only report discarded tasks and scheduling load

	auto avgTaskWaitTime = fTotalTaskWaitTime;
	avgTaskWaitTime /= fTotalTasks;

	out << "{" << endl;
	cerr << "{" << endl;

	out << "\treconfiguration : \"" << fReConfigurationType << "\"," << endl;
	cerr << "\treconfiguration : \"" << fReConfigurationType << "\"," << endl;

	out << "\ttotalTasks : " << fTotalTasks << "," << endl;
	cerr << "\ttotalTasks : " << fTotalTasks << "," << endl;

	out << "\ttotalNodes : " << fTotalNodes << "," << endl;
	cerr << "\ttotalNodes : " << fTotalNodes << "," << endl;

	out << "\ttotalConfigs : " << fTotalConfigs << "," << endl;
	cerr << "\ttotalConfigs : " << fTotalConfigs << "," << endl;

	out << "\tsearchLength : " << fTotalSearchLengthScheduler << "," << endl;
	cerr << "\tsearchLength : " << fTotalSearchLengthScheduler << "," << endl;

	out << "\tdiscardedTasks : " << fTotalDiscardedTasks << "," << endl;
	cerr << "\tdiscardedTasks : " << fTotalDiscardedTasks << "," << endl;

	out << "\tavgTaskWaitTime : " << avgTaskWaitTime << "," << endl;
	cerr << "\tavgTaskWaitTime : " << avgTaskWaitTime << "," << endl;

	out << "\tsimulationTime : " << fSimulationTime << "," << endl;
	cerr << "\tsimulationTime : " << fSimulationTime << "," << endl;

	out << "}," << endl;
	cerr << "}," << endl;

	out.close();

	//system("PAUSE");
}

static void ReadInt(ifstream& in, int& outParam) {
	string s = "#";
	
	while (s[0] == '#') {
		getline(in, s);
	}

	//read the integer parameter from this string
	istringstream iss(s);

	iss >> outParam;
}

void Simulator::ReadData() {
	//read the simulation data from data.in file

	cerr << "Reading simulation data .." << endl;

	ifstream in("Resources/data.in");

	//read lines from this file
	//if started with # skip it it is a comment

	//read the total tasks
	ReadInt(in, fTotalTasks);

	cerr << "total tasks = " << fTotalTasks << endl;

	//read the total nodes
	ReadInt(in, fTotalNodes);

	cerr << "total nodes = " << fTotalNodes << endl;
	
	//read the total configs
	ReadInt(in, fTotalConfigs);

	cerr << "total configs = " << fTotalConfigs << endl;
	
	//read the next task min/max interval
	ReadInt(in, fNextTaskMinInterval);
	ReadInt(in, fNextTaskMaxInterval);

	cerr << "next task interval = [" << fNextTaskMinInterval << ", " << fNextTaskMaxInterval << "]" << endl;
	
	//task low/high area in [100..2500]
	ReadInt(in, fTaskLowArea);
	ReadInt(in, fTaskHighArea);

	cerr << "task area interval = [" << fTaskLowArea << ", " << fTaskHighArea << "]" << endl;
	
	//node low/high area in [1000..5000]
	ReadInt(in, fNodeLowArea);
	ReadInt(in, fNodeHighArea);

	cerr << "node area interval = [" << fNodeLowArea << ", " << fNodeHighArea << "]" << endl;
	
	//task required time low/high in [100..10000]
	ReadInt(in, fTaskReqTimeLow);
	ReadInt(in, fTaskReqTimeHigh);

	cerr << "task required time interval = [" << fTaskReqTimeLow << ", " << fTaskReqTimeHigh << "]" << endl;
	
	//config time low/high in [1..5]
	ReadInt(in, fConfigTimeLow);
	ReadInt(in, fConfigTimeHigh);

	cerr << "config time interval = [" << fConfigTimeLow << ", " << fConfigTimeHigh << "]" << endl;

	//multiple configurations allowed 0 / 1
	int t;
	ReadInt(in, t);

	if (t == 1) {
		cerr << "Multiple configuration per node allowed .." << endl;
		fScheduler = new Scheduler(ePartial);
	} else {
		cerr << "One configuration per node allowed .." << endl;
		fScheduler = new Scheduler(eFull);
	}

	in.close();

	//system("PAUSE");

	//Closest Match ratio
	//fClosestMatchRatio = 0.1;
}
 
//init parameters that will be reported in MakeReport function
void Simulator::Init() {
	fTotalCompletedTasks = 0;
	fTotalCurGenTasks = 0;
	fTotalCurSusTasks = 0;
	fTotalWastedArea = 0;
	fTotalSearchLengthScheduler = 0;
	fTotalTaskWaitTime = 0;
	fTotalTasksRunningTime = 0;
	fTotalConfigurationTime = 0;
}
