#include "Task.h"
#include "Config.h"
#include "TimeManager.h"

using namespace std;

map<int, Task*> Task::sTasksMap;

uLONG64 Task::sTotalTaskWaitingTime = 0;

Task::Task(int inTaskId, int inPrefConfig, int inCreateTime, int inRequiredTime) :
fTaskId(inTaskId),
fPrefConfig(inPrefConfig),
fCreateTime(inCreateTime),
fRequiredTime(inRequiredTime),
fAssignedConfig(-1),
fStartTime(-1),
fCompletionTime(-1),
fRunningTime(0),
fSuspendRetry(10),
fCurConfigTime(0){
	sTasksMap[inTaskId] = this;
	fConfigTime = Config::GetConfigById(inPrefConfig)->GetConfigTime();
}

Task::~Task() {
	DoOnEndJob();
}

int Task::GetRequiredArea() const {
	auto config = Config::GetConfigById(fPrefConfig);
	int requiredArea = config->GetRequiredArea();

	return requiredArea;
}

void Task::Start() {
	fStartTime = TimeManager::GetInstance()->GetCurrentTime();
	fAssignedConfig = fPrefConfig;
}

Task* Task::GetTaskById(int inTaskId) {
	auto it = sTasksMap.find(inTaskId);
	if(it != sTasksMap.end()) {
		return it->second;
	} else {
		return NULL;
	}
}

void Task::Update() {
	if (fCurConfigTime < fConfigTime) {
		++fCurConfigTime;
	}
	else {
		++fRunningTime;
	}
}

void Task::DoOnEndJob() {
	//report some statistics that will be used to generate the final report
}

void Task::Clear() {
	sTotalTaskWaitingTime = 0;
}