#ifndef __SCHEDULER__
#define __SCHEDULER__

#include <list>

class Node;
class Task;

enum ReConfigurationType {
	eFull = 0,
	ePartial
};

typedef unsigned long long uLONG64;

class Scheduler {
public:
	Scheduler(ReConfigurationType inReConfigurationType);
	~Scheduler();

	//schedule this task by putting it in one of inNodes list
	//or put it in the suspended tasks list
	void Schedule(Task* inTask, std::list<Node*>& inNodes);

	void Discard(Task* inTask);

	//re-schedule suspended tasks list
	void RescheduleSuspendedTasks(std::list<Node*>& inNodes);
	
	//get suspended tasks count
	int GetSuspendedTasksCount() const;

	//get total search length
	uLONG64 GetTotalSearchLength() const { return fTotalSearchLengthScheduler; }

	//get total discarded tasks
	uLONG64 GetTotalDiscardedTasks() const { return fTotalDiscardedTasks; }

	//get reconfiguration type
	ReConfigurationType GetReConfigurationType() { return fReConfigurationType; }

private:
	void ScheduleWithFullReConfiguration(Task* inTask, std::list<Node*>& inNodes);
	void ScheduleWithPartialReConfiguration(Task* inTask, std::list<Node*>& inNodes);

	std::list<Task*> fSuspendedTasks;
	uLONG64 fTotalSearchLengthScheduler;
	uLONG64 fTotalDiscardedTasks;

	ReConfigurationType fReConfigurationType;
};

#endif
