#include <fstream>

#include "Simulator.h"

using namespace std;

int main() {

	//generate differents total tasks values with partial configuration
	for (int t = 5000; t <= 100000; t += 5000) {
		ofstream out("Resources/data.in");
		out << "#total tasks from 1000 --> 100000" << endl;
		out << t << endl;
		out << "#total nodes in{ 8, 16, 32, 64, 100, 200, 300, 500, 1000 }" << endl;
		out << 100 << endl;
		out << "#total configs in{ 10, 20, 30, 40, 50 }" << endl;
		out << 50 << endl;
		out << "#read the next task min / max interval in[1..50] and[100..1000]" << endl;
		out << 1 << endl;
		out << 50 << endl;
		out << "#task low / high area in[100..2500]" << endl;
		out << 100 << endl;
		out << 2500 << endl;
		out << "#node low / high area in[1000..5000]" << endl;
		out << 1000 << endl;
		out << 5000 << endl;
		out << "#task required time low / high" << endl;
		out << 100 << endl;
		out << 10000 << endl;
		out << "#config time low / high in[1..5]" << endl;
		out << 1 << endl;
		out << 5 << endl;
		out << "#multiple configurations allowed 0 / 1" << endl;
		out << 1 << endl;

		out.close();

		Simulator simulator;

		simulator.Run();
		simulator.MakeReport();
	}

	//generate differents total tasks values with full configuration
	for (int t = 5000; t <= 100000; t += 5000) {
		ofstream out("Resources/data.in");
		out << "#total tasks from 1000 --> 100000" << endl;
		out << t << endl;
		out << "#total nodes in{ 8, 16, 32, 64, 100, 200, 300, 500, 1000 }" << endl;
		out << 100 << endl;
		out << "#total configs in{ 10, 20, 30, 40, 50 }" << endl;
		out << 50 << endl;
		out << "#read the next task min / max interval in[1..50] and[100..1000]" << endl;
		out << 1 << endl;
		out << 50 << endl;
		out << "#task low / high area in[100..2500]" << endl;
		out << 100 << endl;
		out << 2500 << endl;
		out << "#node low / high area in[1000..5000]" << endl;
		out << 1000 << endl;
		out << 5000 << endl;
		out << "#task required time low / high" << endl;
		out << 100 << endl;
		out << 10000 << endl;
		out << "#config time low / high in[1..5]" << endl;
		out << 1 << endl;
		out << 5 << endl;
		out << "#multiple configurations allowed 0 / 1" << endl;
		out << 0 << endl;

		out.close();

		Simulator simulator;

		simulator.Run();
		simulator.MakeReport();
	}
	
	return 0;
}
