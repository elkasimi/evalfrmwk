#include "Config.h"

using namespace std;

map<int, Config*> Config::sConfigsMap;

Config::Config(int inConfigId, int inConfigTime, int inRequiredArea) :
fConfigId(inConfigId),
fConfigTime(inConfigTime),
fRequiredArea(inRequiredArea) {
	sConfigsMap[inConfigId] = this;
}

Config::~Config() {}

Config* Config::GetConfigById(int inConfigId) {
	auto it = sConfigsMap.find(inConfigId);
	if(it != sConfigsMap.end()) {
		return it->second;
	} else {
		return NULL;
	}
}

void Config::Clear() {
	sConfigsMap.clear();
}
