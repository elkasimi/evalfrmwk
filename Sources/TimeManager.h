#ifndef __TIME_MANAGER__
#define __TIME_MANAGER__

class TimeManager {
public:
	static TimeManager* GetInstance();
	static void ClearInstance();

	void Tick();
	int GetCurrentTime() const;

private:
	TimeManager();
	~TimeManager();

	int fTicks;
	static TimeManager* sInstance;
};

#endif