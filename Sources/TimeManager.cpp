#include "TimeManager.h"

TimeManager* TimeManager::sInstance = nullptr;

TimeManager::TimeManager() : fTicks(0) {}

TimeManager::~TimeManager() {}

TimeManager* TimeManager::GetInstance() {
	if (sInstance == nullptr) {
		sInstance = new TimeManager();
	}

	return sInstance;
}

void TimeManager::ClearInstance() {
	if (sInstance != nullptr) {
		delete sInstance;
		sInstance = nullptr;
	}
}

void TimeManager::Tick() {
	++fTicks;
}

int TimeManager::GetCurrentTime() const {
	return fTicks;
}
