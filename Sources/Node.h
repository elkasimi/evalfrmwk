#ifndef __NODE__
#define __NODE__

#include <map>
#include <set>
#include <list>

class Task;
class Config;

typedef std::pair<Config*, Task*> ConfigTaskPair;
typedef std::list<ConfigTaskPair> ConfigTaskPairList;

enum State {
	eBlank = 0,
	eBusy,
	eIdle,
};

class Node {
public:
	Node(int inNodeId, int inTotalArea);
	~Node();
	
	State GetState() const;

	int GetId() const { return fId; }
	
	//add a task to the node
	void AddTask(Task* inTask);
	
	//remove a task
	void RemoveTask(Task* inTask);

	//clear a config
	void ClearConfig();
	
	//this will update the tasks list and node status
	void Update();

	int GetTotalArea() const { return fTotalArea; }

	int GetAvailableArea() const { return fAvailableArea; }

	int GetAvailableConfigArea() const { return (fTotalArea - fConfiguredArea); }

	int GetMaxRemainingTime() const { return fMaxRemainingTime; }

	static void Clear();

private:
	friend class Scheduler;

	int fId;
	int fTotalArea;
	int fAvailableArea;
	int fConfiguredArea;
	int fMaxRemainingTime;

	State fState;
	
	//the config ==> task map
	ConfigTaskPairList fConfigTaskPairList;

	static std::map<Config*, std::set<Node*>> sIdleNodes;

	static std::map<Config*, std::set<Node*>> sBusyNodes;
};

#endif
