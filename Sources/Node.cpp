#include <iostream>

#include "Node.h"
#include "Task.h"
#include "Config.h"

using namespace std;

map<Config*, set<Node*>> Node::sIdleNodes;

map<Config*, set<Node*>> Node::sBusyNodes;

Node::Node(int inNodeId, int inTotalArea) :
fId(inNodeId),
fTotalArea(inTotalArea),
fAvailableArea(inTotalArea),
fConfiguredArea(0),
fState(eBlank),
fMaxRemainingTime(0)
{
}

Node::~Node() {}

State Node::GetState() const {
	return fState;
}

void Node::AddTask(Task* inTask) {
	//first try to find the prefered config of the task in config/task list
	auto config = inTask->GetPrefConfig();

	bool configFound = false;
	int requiredArea = inTask->GetRequiredArea();

	for(auto& p : fConfigTaskPairList) {
		//if this pair has the same configuration
		if ((p.first == config) && (p.second == nullptr)) {
			configFound = true;
			p.second = inTask;
			fAvailableArea -= requiredArea;
			break;
		}
	}
	
	if(!configFound) {
		//remove Configs untill there is enough space for this task
		while(GetAvailableConfigArea() < requiredArea) {
			ClearConfig();
		}
		
		fConfiguredArea += requiredArea;
		fAvailableArea -= requiredArea;
		fConfigTaskPairList.push_back(make_pair(config, inTask));
	}
	
	//here update the the busy nodes list of the task configuration
	sBusyNodes[config].insert(this);

	//update node state
	fState = eBusy;

	//start this task
	inTask->Start();

	//add the waiting time of this task
	auto waitingTime = inTask->GetStartTime() - inTask->GetCreateTime() + inTask->GetConfigTime();
	Task::sTotalTaskWaitingTime += waitingTime;
}

void Node::RemoveTask(Task* inTask) {
	for(auto& p : fConfigTaskPairList) {
		if(p.second == inTask) {
			p.second = nullptr;

			//update the available area
			fAvailableArea += inTask->GetRequiredArea();
			break;
		}
	}

	auto config = inTask->GetPrefConfig();

	sBusyNodes[config].erase(this);
	sIdleNodes[config].insert(this);

	//update node state
	if(fAvailableArea == fTotalArea) {
		fState = eIdle;
	} else {
		fState = eBusy;
	}

	delete inTask;
}

void Node::ClearConfig() {
	for(auto it = fConfigTaskPairList.begin(), end = fConfigTaskPairList.end(); it != end; ++it) {
		if(it->second == nullptr) {
			fConfiguredArea -= it->first->GetRequiredArea();
			fConfigTaskPairList.erase(it);
			break;
		}
	}
}

void Node::Update() {
	fMaxRemainingTime = 0;

	for(auto& p : fConfigTaskPairList) {
		//if this pair is associated to a running task
		if (p.second == nullptr) {
			continue;
		}

		auto task = p.second;

		task->Update();

		//update fMaxRemainingTime
		int remainingTime = task->GetRemainingTime();

		if (fMaxRemainingTime < remainingTime) {
			fMaxRemainingTime = remainingTime;
		}

		if(task->IsDone()) {
			//log the information that a task has been completed succesfully
			cerr << "Task of id = " << task->GetId() << " has been completed succesfully" << endl;
			
			RemoveTask(task);
		} else {
			// cerr << "Task of id = " << task->GetId() << " needs " << task->GetRemainingTime() << " ticks." << endl;
		}
	}
}

void Node::Clear() {
	sIdleNodes.clear();
	sBusyNodes.clear();
}