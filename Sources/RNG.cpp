#include <cstdlib>

#include "RNG.h"

int RNG::RandomBetween(int inMin, int inMax) {
	int r = ((inMax - inMin) * rand()) / RAND_MAX;
	r += inMin;

	return r;
}
