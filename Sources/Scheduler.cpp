#include <iostream>

#include "Scheduler.h"
#include "Node.h"
#include "Task.h"

#define OO 1000000000

using namespace std;

Scheduler::Scheduler(ReConfigurationType inReConfigurationType) :
fReConfigurationType(inReConfigurationType),
fTotalSearchLengthScheduler(0),
fTotalDiscardedTasks(0) {
}

Scheduler::~Scheduler() {
}

void Scheduler::Schedule(Task* inTask, list<Node*>& inNodes) {
	if (fReConfigurationType == ePartial)
		ScheduleWithPartialReConfiguration(inTask, inNodes);
	else
		ScheduleWithFullReConfiguration(inTask, inNodes);
}

void Scheduler::Discard(Task* inTask) {
	++fTotalDiscardedTasks;
	delete inTask;
}

void Scheduler::RescheduleSuspendedTasks(list<Node*>& inNodes) {
	auto suspendedTasks = fSuspendedTasks;

	// cerr << "Rescheduling " << fSuspendedTasks.size() << endl;

	fSuspendedTasks.clear();

	for(auto task : suspendedTasks) {
		Schedule(task, inNodes);
	}
}

void Scheduler::ScheduleWithFullReConfiguration(Task* inTask, std::list<Node*>& inNodes) {
	Node* bestNode = NULL;

	int minAvailableArea = OO;
	int requiredArea = inTask->GetRequiredArea();

	//first search on idle nodes that has the task configuration
	auto config = inTask->GetPrefConfig();
	for(auto node : Node::sIdleNodes[config]) {
		int availableArea = node->GetAvailableArea();
		if((requiredArea <= availableArea) && (minAvailableArea > availableArea)){
			minAvailableArea = availableArea;
			bestNode = node;
		}

		//increment search length
		++fTotalSearchLengthScheduler;
	}

	if(bestNode != NULL) {
		bestNode->AddTask(inTask);
		return;
	}


	//second search in blank nodes
	for(auto node : inNodes) {
		if(node->GetState() == eBlank) {
			int availableArea = node->GetAvailableArea();
			if ((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
				minAvailableArea = availableArea;
				bestNode = node;
			}	
		}
		//increment search length
		++fTotalSearchLengthScheduler;
	}
	
	if(bestNode != NULL){
		bestNode->AddTask(inTask);
		return;
	}

	//search in idle nodes for the best match configuration
	for(auto node : inNodes) {
		if(node->GetState() == eIdle) {
			int availableArea = node->GetAvailableArea();
			if ((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
				minAvailableArea = availableArea;
				bestNode = node;
			}
		}

		//increment search length
		++fTotalSearchLengthScheduler;		
	}

	if(bestNode != NULL){
		bestNode->AddTask(inTask);
		return;
	}

	//test if there is a busy node that can hold this task
	bool suspendTask = false;
	for(auto node : inNodes) {
		if(node->GetState() == eBusy) {
			if(requiredArea < node->GetTotalArea()) {
				suspendTask = true;
				break;
			}
		}

		//increment search length
		++fTotalSearchLengthScheduler;		
	}

	if(suspendTask)
		fSuspendedTasks.push_back(inTask);
	else
		Discard(inTask);
}

void Scheduler::ScheduleWithPartialReConfiguration(Task* inTask, std::list<Node*>& inNodes) {
	//first search idle node with prefered config
	Node* bestNode = NULL;

	int minAvailableArea = OO;
	int requiredArea = inTask->GetRequiredArea();

	//first search on idle nodes that has the task configuration
	auto config = inTask->GetPrefConfig();
	for(auto node : Node::sIdleNodes[config]) {
		int availableArea = node->GetAvailableArea();

		if((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
			minAvailableArea = availableArea;
			bestNode = node;
		}

		//increment search length
		++fTotalSearchLengthScheduler;
	}

	if(bestNode != NULL){
		bestNode->AddTask(inTask);
		return;
	}
	
	//second search in blank nodes
	for(auto node : inNodes) {
		if(node->GetState() == eBlank) {
			int availableArea = node->GetAvailableArea();
			if ((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
				minAvailableArea = availableArea;
				bestNode = node;
			}	
		}

		//increment search length
		++fTotalSearchLengthScheduler;		
	}
	
	if(bestNode != NULL){
		bestNode->AddTask(inTask);
		return;
	}
	
	//3rd search best partially blank node
	for(auto node : inNodes) {
		if(node->GetState() == eBusy) {
			int availableArea = node->GetAvailableArea();
			if ((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
				minAvailableArea = availableArea;
				bestNode = node;
			}	
		}

		//increment search length
		++fTotalSearchLengthScheduler;		
	}

	if(bestNode != NULL) {
		bestNode->AddTask(inTask);
		return;
	}

	//4th search idle node with sufficient area
	for(auto node : inNodes) {
		if(node->GetState() == eIdle) {
			int availableArea = node->GetAvailableArea();
			if ((requiredArea <= availableArea) && (minAvailableArea > availableArea)) {
				minAvailableArea = availableArea;
				bestNode = node;
			}	
		}
		//increment search length
		++fTotalSearchLengthScheduler;		
	}

	if(bestNode != NULL) {
		bestNode->AddTask(inTask);
		return;
	}

	//5th search for busy node candidate
	bool suspendTask = false;
	for(auto node : inNodes) {
		if(node->GetState() == eBusy) {
			if(requiredArea <= node->GetTotalArea()) {
				suspendTask = true;
				break;
			}
		}

		//increment search length
		++fTotalSearchLengthScheduler;		
	}

	if(suspendTask)
		fSuspendedTasks.push_back(inTask);
	else
		//if no busy availible discard task
		Discard(inTask);
}

//get suspended tasks count
int Scheduler::GetSuspendedTasksCount() const {
	int count = (int) fSuspendedTasks.size();
	return count;
}
