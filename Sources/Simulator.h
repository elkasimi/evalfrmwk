#ifndef __DREAM_SIM__
#define __DREAM_SIM__

#include <list>
#include <string>

class Task;
class Node;
class Scheduler;

typedef unsigned long long uLONG64;

class Simulator {
public:
	Simulator();
	~Simulator();

	void Run();
	void MakeReport();

private:
	void ReadData();
	void Init();
	
	int fTimeTick;
	int fTotalTasks;
	int fTotalConfigs;
	int fTotalNodes;
	int fTotalCompletedTasks;
	int fTotalCurGenTasks;
	int fTotalCurSusTasks;
	int fTotalWastedArea;
	int fTotalTasksRunningTime;
	int fTotalConfigurationTime;
	int fNextTaskMinInterval;
	int fNextTaskMaxInterval;
	double fClosestMatchRatio;
	int fNodeLowArea, fNodeHighArea;
	int fTaskLowArea, fTaskHighArea;
	int fTaskReqTimeLow, fTaskReqTimeHigh;
	int fConfigTimeLow, fConfigTimeHigh;

	uLONG64 fTotalDiscardedTasks;
	uLONG64 fTotalSearchLengthScheduler;
	uLONG64 fSimulationTime;
	uLONG64 fTotalTaskWaitTime;

	std::string fReConfigurationType;

	Scheduler* fScheduler;
};

#endif
