#ifndef __TASK__
#define __TASK__

#include <map>

#include "Config.h"

typedef unsigned long long uLONG64;

class Task {
public:
	Task(int inTaskId, int inPrefConfig, int inCreatedTime, int inRequiredTime);
	~Task();

	int GetId() { return fTaskId; }
	int GetPrefConfigId() { return fPrefConfig; }
	Config* GetPrefConfig() { return Config::GetConfigById(fPrefConfig); }
	int GetRequiredArea() const;
	int GetRemainingTime() const { return (fRequiredTime - fRunningTime); }
	void Start();
	void Update();
	bool IsDone() const { return (fRunningTime == fRequiredTime); }
	int GetStartTime() const { return fStartTime; }
	int GetCreateTime() const { return fCreateTime; }
	int GetConfigTime() const { return fConfigTime; }

	static Task* GetTaskById(int inTaskId);

	static uLONG64 sTotalTaskWaitingTime;

	static void Clear();

private:
	void DoOnEndJob();

	int fTaskId;
	int fPrefConfig;
	int fAssignedConfig;
	int fCreateTime;
	int fStartTime;
	int fCompletionTime;
	int fRequiredTime;
	int fSuspendRetry;
	int fRunningTime;
	int fConfigTime;
	int fCurConfigTime;

	static std::map<int, Task*> sTasksMap;
};

#endif
