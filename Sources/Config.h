#ifndef __CONFIG__
#define __CONFIG__

#include <map>

class Config {
public:
	Config(int inConfigId, int inConfigTime, int inRequiredArea);
	~Config();

	int GetRequiredArea() const { return fRequiredArea; }
	int GetConfigTime() const { return fConfigTime; }

	static Config* GetConfigById(int inConfigId);

	static void Clear();

private:
	int fConfigId;
	int fConfigTime;
	int fRequiredArea;

	static std::map<int, Config*> sConfigsMap;
};

#endif
